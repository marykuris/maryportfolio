(function() {
    jQuery(document).ready(function() {
        var s;
        return s = function() {
            return $(this).toggleClass("js-hover-active"), $(this).parent().find("article").toggleClass("js-hover");
        }, $(".portfolio-list").on("mouseenter", ".portfolio-item", s), $(".portfolio-list").on("mouseleave", ".portfolio-item", s), $(window).scroll(function() {
            var s, e;
            return e = $(document).scrollTop(), s = $("header").find("div"), e > 60 && !s.hasClass("js-nav") ? s.addClass("js-nav") : 60 >= e && s.hasClass("js-nav") ? $(".js-nav").removeClass("js-nav") : void 0;
        });
    });
}).call(this);